package com.twu.refactoring;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class DateParser {
    private final String dateAndTimeString;
    private static final HashMap<String, TimeZone> KNOWN_TIME_ZONES = new HashMap<String, TimeZone>();

    static {
        KNOWN_TIME_ZONES.put("UTC", TimeZone.getTimeZone("UTC"));
    }

    /**
     * Takes a date in ISO 8601 format and returns a date
     *
     * @param dateAndTimeString - should be in format ISO 8601 format
     *                          examples -
     *                          2012-06-17 is 17th June 2012 - 00:00 in UTC TimeZone
     *                          2012-06-17TZ is 17th June 2012 - 00:00 in UTC TimeZone
     *                          2012-06-17T15:00Z is 17th June 2012 - 15:00 in UTC TimeZone
     */
    DateParser(String dateAndTimeString) {
        this.dateAndTimeString = dateAndTimeString;
    }

    public Date parse() {
        int year, month, date, hour, minute;

        year = DateValidator.yearValidator(dateAndTimeString);
        month = DateValidator.monthValidator(dateAndTimeString);
        date = DateValidator.dayValidator(dateAndTimeString);

        if (isZtimePart(dateAndTimeString)) {
            hour = 0;
            minute = 0;
        } else {
            hour = DateValidator.hourValidator(dateAndTimeString);
            minute = DateValidator.minuteValidator(dateAndTimeString);
        }

        Calendar calendar = getCalendar(year, month, date, hour, minute);
        return calendar.getTime();
    }

    private Boolean isZtimePart(String dateAndTimeString) {
        return dateAndTimeString.substring(11, 12).equals("Z");
    }

    private Calendar getCalendar(int year, int month, int date, int hour, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.set(year, month - 1, date, hour, minute, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }
}
