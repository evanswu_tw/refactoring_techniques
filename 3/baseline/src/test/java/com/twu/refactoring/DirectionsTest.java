package com.twu.refactoring;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DirectionsTest {

    @Test
    public void shouldReturnWestWhenTurningLeftFromNorth() {
        assertEquals(Directions.N.left(), Directions.W);
    }
    @Test
    public void shouldReturnEastTurningRightFromNorth() {
        assertEquals(Directions.N.right(), Directions.E);
    }

}
