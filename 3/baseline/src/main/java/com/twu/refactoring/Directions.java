package com.twu.refactoring;

public enum Directions {
    N,
    E,
    S,
    W;

    public Directions left() {
        return getCardinalDirection(W, N, E, S);
    }

    public Directions right() {
        return getCardinalDirection(E, S, W, N);
    }

    private Directions getCardinalDirection(Directions e, Directions s, Directions w, Directions n) {
        switch (this) {
            case N:
                return e;
            case E:
                return s;
            case S:
                return w;
            case W:
                return n;
            default:
                return null;
        }
    }


}