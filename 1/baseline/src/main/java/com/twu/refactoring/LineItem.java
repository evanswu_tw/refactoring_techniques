package com.twu.refactoring;

public class LineItem {
    private String description;
    private double price;
    private int quality;

    // its important to put scope here for the constructor, in this case i assume this is public
    //
    public LineItem(String description, double price, int quality) {
        super();
        this.description = description;
        this.price = price;
        this.quality = quality;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quality;
    }

    public double totalAmount() {
        return price * quality;
    }

    public double getSalesTax(double taxRatio) {
        return totalAmount() * taxRatio;
    }


    public void toFormattedBodyString(StringBuilder output) {
        String separeter = "\t";
        String end = "\n";
        output.append(getDescription());
        output.append(separeter)
                .append(getPrice())
                .append(separeter)
                .append(getQuantity())
                .append(separeter)
                .append(totalAmount())
                .append(end);
    }

}
