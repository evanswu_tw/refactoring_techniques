package com.twu.refactoring;

import java.util.List;

public class Order {
    private String name;
    private String address;
    private List<LineItem> lineItems;

    private double totalSalesTax = 0d;
    private double total = 0d;

    public void setTaxRatio(double taxRatio) {
        this.taxRatio = taxRatio;
    }

    private double taxRatio = 0;

    // once again scoping the contructor is important
    public Order(String name, String address, List<LineItem> lineItems) {
        this.name = name;
        this.address = address;
        this.lineItems = lineItems;
    }

    public String getCustomerName() {
        return name;
    }

    public String getCustomerAddress() {
        return address;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void toFormattedOrderHeader(StringBuilder output) {
        output.append(getCustomerName())
                .append(getCustomerAddress())
                .append("\n");
    }

    public void toFormattedOrderBody(StringBuilder output) {
        for (LineItem lineItem : lineItems) {
            lineItem.toFormattedBodyString(output);
        }
    }

    public void calculateOrder() {
        double salesTax;
        for (LineItem lineItem : lineItems) {
            salesTax = lineItem.getSalesTax(taxRatio);
            total += lineItem.totalAmount() + salesTax;
            totalSalesTax += salesTax;
        }
    }

    public double getTotalSalesTax() {
        return totalSalesTax;
    }

    public double getTotal() {
        return total;
    }


}
