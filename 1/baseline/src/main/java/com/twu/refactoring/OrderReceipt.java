package com.twu.refactoring;

/**
 * OrderReceipt prints the details of order including customer name, address, description, quantity,
 * price and amount. It also calculates the sales tax @ 10% and prints as part
 * of order. It computes the total order amount (amount of individual lineItems +
 * total sales tax) and prints it.
 */
public class OrderReceipt {
    private Order order;

    OrderReceipt(Order order) {
        this.order = order;
    }

    public String printReceipt() {
        StringBuilder output = new StringBuilder();

        order.setTaxRatio(0.1);
        order.calculateOrder();

        printHeader(output,order);
        printBody(output,order);
        printTails(output, order);

        return output.toString();
    }

    private void printHeader(StringBuilder output,Order order) {
        output.append("======Printing Orders======\n");
        order.toFormattedOrderHeader(output);
    }


    private void printBody(StringBuilder output, Order order) {
        order.toFormattedOrderBody(output);
    }

    private void printTails(StringBuilder output, Order order) {
        double totalSalesTax = order.getTotalSalesTax();
        double total = order.getTotal();
        output.append("Sales Tax").append("\t").append(totalSalesTax);
        output.append("Total Amount").append("\t").append(total);
    }
}
